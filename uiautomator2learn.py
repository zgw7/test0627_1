import time

import uiautomator2 as u2

# print("################  IP   ##################")
# d1 = u2.connect('10.224.206.148')
# print(d1.info)

#
# d3 = u2.connect_wifi('10.224.206.148')
# print(d3.info)

# print("################  Serise Number SN   ##################")
# d2 = u2.connect('4b9dc0e4')
# print(d2.info)

# print("########### adb_Wifi ############")
# d4 = u2.connect('10.224.206.148:5555')
# print(d4.info)

# d2.healthcheck()
#
# d2.screenshot("bbb.jpg")
# d2.app_stop_all()





########### Debug HTTP Request ###########
print("################  Serise Number SN   ##################")
d = u2.connect('4b9dc0e4')
print(d.info)
# d.debug = True
# print(d.info)


# ##############  Implicit wait ##########
# d.implicitly_wait(10.0)
# d(text="百度地图").click()
# print("wait timeout", d.implicitly_wait())

# d.app_start("com.netease.cloudmusic")
# info = d.app_info("com.netease.cloudmusic")
# print(info)
# d.app_stop_all(excludes=["com.netease.cloudmusic"])
#
# d.app_stop("com.netease.cloudmusic")

# img = d.app_icon("com.netease.cloudmusic")
# img.save("icon_netease.jpg")

# # push to a folder
# result1 = d.push("foo.txt", "/sdcard/")
# print(result1)
# # push and rename
# d.push("foo.txt", "/sdcard/bar.txt")
# # push fileobj
# with open("foo.txt", 'rb') as f:
#     d.push(f, "/sdcard/")
# # push and change file access mode
# d.push("foo.sh", "/data/local/tmp/", mode=0o755)

# d.pull(src="/sdcard/foo.txt", dst="foo1.txt")

# output, exit_code = d.shell("pwd", timeout=60)
# print(output)
# print(exit_code)
#
# output, exit_code = d.shell(['ls', '-l'])
# print(output)
# print(exit_code)

# r = d.shell("logcat", stream=True)
# # r: requests.models.Response
# deadline = time.time() + 10 # run maxium 10s
# try:
#     for line in r.iter_lines(): # r.iter_lines(chunk_size=512, decode_unicode=None, delimiter=None)
#         if time.time() > deadline:
#             break
#         print("Read:", line.decode('utf-8'))
# finally:
#     r.close() # this method must be called

# sess = d.session("com.netease.cloudmusic")
# sess = d.session("com.duokan.reader")
# print(sess)
# sess.close()

# with d.session("com.netease.cloudmusic") as sess:
#     sess(text="Play").click()

# sess = d.session('com.netease.cloudmusic', attach=True)
# time.sleep(5)
# sess.close()

# print(d.info)
# print(d.window_size())
# print(d.current_app())

# sess = d.session('com.duokan.reader')
# time.sleep(5)
# print(d.wait_activity('com.duokan.reader..DkMainActivity'))

# print(d.serial)
# print(d.wlan_ip)
# print(d.device_info)

# d.click(0.471, 0.877)
# d.double_click()
# d.long_click()
# d.swipe()
# d.drag()
# d.swipe_points()
# d.swipe((0.228, 0.603), (0.501, 0.606), (0.766, 0.603), (0.501, 0.729),  0.2)

# print(d.orientation)
# d.set_orientation('l')
# d.set_orientation('r')
# d.set_orientation('n')

# d.screenshot("home.jpg")

# import cv2
# image = d.screenshot(format='opencv')
# cv2.imwrite('home1.jpg', image)

# xml = d.dump_hierarchy()
# print(xml)
# d.open_notification()
# d.open_quick_settings()

# d(resourceId="com.miui.home:id/icon_icon", description="游戏中心").click()

# info = d(resourceId="com.miui.home:id/icon_icon", description="设置").info
# print(info)

# d(resourceId="com.tencent.mm:id/b49").set_text("hi,this is test~")
# print(d(resourceId="com.tencent.mm:id/b49").get_text())
# d(resourceId="com.tencent.mm:id/b49").clear_text()

# x,y = d(text="高德请客1分钱吃烤肉").center()
# print("x = " + x)
# print("y = " + y)
#
# x,y = d(text="高德请客1分钱吃烤肉").center(offset=(0,0))
# print("x = " + x)
# print("y = " + y)

# d(text="高德请客1分钱吃烤肉").click(timeout=3)

# d(text="搜索系统设置项").click(timeout=3)

# d(text="搜索系统设置项").click(offset=(1,1))

# d(text="AAA").click_exists(2)
# d(text="AAA").click(2)

# d(scrollable=True).scroll(steps=10)
# d(scrollable=True).fling()
# d(scrollable=True).scroll.vert.backward(10)
#
#
# d(scrollable=True).scroll.toEnd()

# print(d.jsonrpc.method)

# d.set_fastinput_ime(True) # 切换成FastInputIME输入法
# d.send_keys("你好123abcEFG") # adb广播输入
# d.clear_text() # 清除输入框所有内容(Require android-uiautomator.apk version >= 1.0.7)
# d.set_fastinput_ime(False) # 切换成正常的输入法
# d.send_action("search") # 模拟输入法的搜索

# d.toast.show("Hello world")
d.toast.show("Hello world", 100.0) # show for 1.0s, default 1.0s





